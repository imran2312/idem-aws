import pytest_asyncio


@pytest_asyncio.fixture
async def aws_iam_user_keys(hub, ctx, aws_iam_user):
    user_name = aws_iam_user["name"]
    create = await hub.exec.aws.iam.access_key.create(ctx, user_name=user_name)
    assert create["result"], create["comment"]

    yield create["ret"]

    keys_list = await hub.exec.aws.iam.access_key.list(ctx, user_name=user_name)
    assert keys_list["result"], keys_list["comment"]
    for key in keys_list["ret"]:
        ret = await hub.exec.aws.iam.access_key.delete(
            ctx, user_name=user_name, access_key_id=key["access_key_id"]
        )
        assert ret["result"], ret["comment"]


async def test_delete_already_deleted(hub, ctx, aws_iam_user_keys):
    delete_never_existed = await hub.exec.aws.iam.access_key.delete(
        ctx,
        access_key_id="AKIAX2FJ77DC5FZ11111",
        user_name=aws_iam_user_keys["user_name"],
    )
    assert delete_never_existed["result"]
    delete_no_user = await hub.exec.aws.iam.access_key.delete(
        ctx, access_key_id=aws_iam_user_keys["access_key_id"], user_name="NOSUCHUSER"
    )
    assert delete_no_user["result"]
    delete_exists = await hub.exec.aws.iam.access_key.delete(
        ctx,
        access_key_id=aws_iam_user_keys["access_key_id"],
        user_name=aws_iam_user_keys["user_name"],
    )
    assert delete_exists["result"]
    delete_again = await hub.exec.aws.iam.access_key.delete(
        ctx,
        access_key_id=aws_iam_user_keys["access_key_id"],
        user_name=aws_iam_user_keys["user_name"],
    )
    assert delete_again["result"]
