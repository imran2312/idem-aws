import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_ec2_ami):
    ami_get_name = "idem-test-exec-get-ami-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.ami.get(
        ctx,
        name=ami_get_name,
        resource_id=aws_ec2_ami["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_ec2_ami["name"] == resource.get("name")
    assert aws_ec2_ami["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    ami_get_name = "idem-test-exec-get-ami-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.ami.get(
        ctx,
        name=ami_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.ami '{ami_get_name}' result is empty" in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_ec2_ami):
    ami_list_name = "idem-test-exec-list-ami-" + str(int(time.time()))
    filters = [{"name": "name", "values": [aws_ec2_ami["name"]]}]
    ret = await hub.exec.aws.ec2.ami.list(ctx, name=ami_list_name, filters=filters)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert aws_ec2_ami["resource_id"] == resource.get("resource_id")
