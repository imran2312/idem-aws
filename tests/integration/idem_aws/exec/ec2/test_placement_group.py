import time
from typing import Any
from typing import Dict

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_placement_group(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 placement group for a module that needs it
    :return: a description of an ec2 placement group
    """
    placement_group_temp_name = "idem-fixture-placement-group-" + str(int(time.time()))
    ret = await hub.states.aws.ec2.placement_group.present(
        ctx,
        name=placement_group_temp_name,
        strategy="cluster",
        tags={"Name": placement_group_temp_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = hub.tool.boto3.resource.create(
        ctx, "ec2", "PlacementGroup", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.placement_group.absent(
        ctx, name=placement_group_temp_name, resource_id=after["GroupName"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get(hub, ctx, aws_ec2_placement_group):
    placement_group_get_name = "idem-test-exec-get-placement-group-" + str(
        int(time.time())
    )
    placement_group_fixture = (
        hub.tool.aws.ec2.conversion_utils.convert_raw_placement_group_to_present(
            raw_resource=aws_ec2_placement_group
        )
    )
    ret = await hub.exec.aws.ec2.placement_group.get(
        ctx,
        name=placement_group_get_name,
        resource_id=placement_group_fixture["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert placement_group_fixture["resource_id"] == resource.get("resource_id")
    assert placement_group_fixture["group_id"] == resource.get("group_id")
    assert resource.get("strategy") == "cluster"


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_resource_id(hub, ctx):
    placement_group_get_name = "idem-test-exec-get-placement-group-" + str(
        int(time.time())
    )
    ret = await hub.exec.aws.ec2.placement_group.get(
        ctx,
        name=placement_group_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"Get aws.ec2.placement_group '{placement_group_get_name}' result is empty"
        in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_list(hub, ctx, aws_ec2_placement_group):
    placement_group_list_name = "idem-test-exec-list-placement-group-" + str(
        int(time.time())
    )
    placement_group_fixture = (
        hub.tool.aws.ec2.conversion_utils.convert_raw_placement_group_to_present(
            raw_resource=aws_ec2_placement_group
        )
    )

    filters = [
        {"name": "group-name", "values": [placement_group_fixture["group_name"]]}
    ]

    ret = await hub.exec.aws.ec2.placement_group.list(
        ctx,
        name=placement_group_list_name,
        filters=filters,
    )

    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], list)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert placement_group_fixture["resource_id"] == resource.get("resource_id")
