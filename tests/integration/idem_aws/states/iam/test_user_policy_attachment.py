import time
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_iam_user, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["policy_arn"] = hub.tool.aws.arn_utils.build(
        service="iam",
        account_id="aws",
        resource="policy/ReadOnlyAccess",
    )
    PARAMETER["user_name"] = aws_iam_user.get("resource_id")
    PARAMETER["name"] = f"{PARAMETER['user_name']}-{str(int(time.time()))}"

    ret = await hub.states.aws.iam.user_policy_attachment.present(ctx, **PARAMETER)
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.iam.user_policy_attachment", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.iam.user_policy_attachment", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["user_name"] == resource.get("user_name")
    assert PARAMETER["policy_arn"] == resource.get("policy_arn")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.iam.user_policy_attachment.describe(ctx)
    resource_key = f"{PARAMETER['user_name']}-{PARAMETER['policy_arn']}"
    assert resource_key in describe_ret
    # Verify that the describe output format is correct
    assert "aws.iam.user_policy_attachment.present" in describe_ret[resource_key]
    described_resource = describe_ret[resource_key].get(
        "aws.iam.user_policy_attachment.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["user_name"] == described_resource_map.get("user_name")
    assert PARAMETER["policy_arn"] == described_resource_map.get("policy_arn")


@pytest.mark.asyncio
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update_no_op(hub, ctx, __test):
    ret = await hub.states.aws.iam.user_policy_attachment.present(ctx, **PARAMETER)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.iam.user_policy_attachment", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["user_name"] == old_resource.get("user_name")
    assert PARAMETER["policy_arn"] == old_resource.get("policy_arn")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    resource = ret["new_state"]
    assert ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["user_name"] == resource.get("user_name")
    assert PARAMETER["policy_arn"] == resource.get("policy_arn")
    assert PARAMETER["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.user_policy_attachment.absent(
        ctx,
        name=PARAMETER["name"],
        user_name=PARAMETER["user_name"],
        policy_arn=PARAMETER["policy_arn"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["user_name"] == old_resource.get("user_name")
    assert PARAMETER["policy_arn"] == old_resource.get("policy_arn")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.iam.user_policy_attachment", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.iam.user_policy_attachment", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.user_policy_attachment.absent(
        ctx,
        name=PARAMETER["name"],
        user_name=PARAMETER["user_name"],
        policy_arn=PARAMETER["policy_arn"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.iam.user_policy_attachment", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.iam.user_policy_attachment.absent(
            ctx,
            name=PARAMETER["name"],
            user_name=PARAMETER["user_name"],
            policy_arn=PARAMETER["policy_arn"],
        )
        assert ret["result"], ret["comment"]
