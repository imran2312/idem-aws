import copy
import time
import uuid
from collections import ChainMap

import pytest
from OpenSSL import crypto


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_certificate_manager(hub, ctx):
    name = "idem-test-acm" + str(uuid.uuid4())
    domain_name = name + ".example.com"
    validation_method = "DNS"
    subject_alternative_names = [name + ".example.net"]
    options = {"CertificateTransparencyLoggingPreference": "ENABLED"}
    tags = [{"Key": "Name", "Value": name}]

    # Request Certificate with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.acm.certificate_manager.present(
        test_ctx,
        name=name,
        domain_name=domain_name,
        validation_method=validation_method,
        subject_alternative_names=subject_alternative_names,
        domain_validation_options=[
            {"DomainName": domain_name, "ValidationDomain": "example.com"},
        ],
        options=options,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.acm.certificate_manager '{name}'" in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert domain_name == ret["new_state"]["domain_name"]
    for alternative_name in subject_alternative_names:
        assert alternative_name in ret["new_state"]["subject_alternative_names"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"]["tags"]), tags
    )
    assert validation_method == ret["new_state"]["validation_method"]
    assert options == ret["new_state"]["options"]

    # Request Certificate
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx,
        name=name,
        domain_name=domain_name,
        validation_method=validation_method,
        subject_alternative_names=subject_alternative_names,
        domain_validation_options=[
            {"DomainName": domain_name, "ValidationDomain": "example.com"},
        ],
        options=options,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert domain_name == ret["new_state"]["domain_name"]
    for alternative_name in subject_alternative_names:
        assert alternative_name in ret["new_state"]["subject_alternative_names"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"]["tags"]), tags
    )
    assert validation_method == ret["new_state"]["validation_method"]
    assert options == ret["new_state"]["options"]
    certificate_arn_requested = ret["new_state"]["resource_id"]

    # If neither of domain_name, private_key or resource_id is given as input, error should be logged.
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx,
        name=name,
        validation_method=validation_method,
        subject_alternative_names=subject_alternative_names,
        domain_validation_options=[
            {"DomainName": domain_name, "ValidationDomain": "example.com"},
        ],
        options={"CertificateTransparencyLoggingPreference": "ENABLED"},
        tags=tags,
    )
    assert not ret["result"], ret["comment"]
    assert (
        "AWS Certificate must be imported (private_key) or created (domain_name)"
        in ret["comment"]
    )

    # Generating Private_Key and Certificate to test importing of certificate
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)

    # Create a self-signed certificate
    validity = 10 * 365 * 24 * 60 * 60
    cert = crypto.X509()
    cert.get_subject().C = "UK"
    cert.get_subject().ST = "London"
    cert.get_subject().L = "London"
    cert.get_subject().O = "Dummy Company Ltd"
    cert.get_subject().OU = "Dummy Company Ltd"
    cert.get_subject().CN = "commonName"
    cert.set_serial_number(1000)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(validity)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, "sha1")

    # Import Certificate with test flag
    ret = await hub.states.aws.acm.certificate_manager.present(
        test_ctx,
        name=name,
        private_key=crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode("utf-8"),
        certificate=crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode("utf-8"),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"]["tags"]), tags
    )

    # Import Certificate
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx=ctx,
        name=name,
        private_key=crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode("utf-8"),
        certificate=crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode("utf-8"),
        tags=tags,
    )
    certificate_arn_imported = ret["new_state"]["resource_id"]
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"]["tags"]), tags
    )

    # this is to ensure the certificates are imported/created in AWS and are returned with AWS get certificates API.
    # certificate_manager support reconciliation via is_pending plugin, but reconciliation is not triggered for tests
    if not hub.tool.utils.is_running_localstack(ctx):
        time.sleep(45)

    # Describe certificate
    describe_ret = await hub.states.aws.acm.certificate_manager.describe(ctx)

    # Verify that both imported and requested certificate are present
    assert certificate_arn_imported in describe_ret
    assert certificate_arn_requested in describe_ret

    # Verify that the describe output format is correct for requested certificate
    assert "aws.acm.certificate_manager.present" in describe_ret.get(
        certificate_arn_requested
    )
    described_resource = describe_ret.get(certificate_arn_requested).get(
        "aws.acm.certificate_manager.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            described_resource_map.get("tags")
        ),
        tags,
    )

    # Verify that the describe output format is correct for imported certificate
    assert "aws.acm.certificate_manager.present" in describe_ret.get(
        certificate_arn_imported
    )
    described_resource = describe_ret.get(certificate_arn_imported).get(
        "aws.acm.certificate_manager.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            described_resource_map.get("tags")
        ),
        tags,
    )

    # Reimport certificate
    # Generating Private_Key and Certificate to test Re-importing of certificate
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)

    # Create a self-signed certificate
    validity = 10 * 365 * 24 * 60 * 60
    cert = crypto.X509()
    cert.get_subject().C = "UK"
    cert.get_subject().ST = "London"
    cert.get_subject().L = "London"
    cert.get_subject().O = "Dummy Company Ltd"
    cert.get_subject().OU = "Dummy Company Ltd"
    cert.get_subject().CN = "commonName"
    cert.set_serial_number(1000)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(validity)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, "sha1")

    new_tags = [{"Key": "Name", "Value": "Updated"}]

    # Reimport certificate and update tags with test flag
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx=test_ctx,
        name=name,
        resource_id=certificate_arn_imported,
        private_key=crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode("utf-8"),
        certificate=crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode("utf-8"),
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret.get("old_state").get("resource_id") == ret.get("new_state").get(
        "resource_id"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"]["tags"]),
        new_tags,
    )
    assert (
        f"Would reimport certificate for aws.acm.certificate_manager {name}"
        in ret["comment"]
    )
    assert f"Would update tags for aws.acm.certificate_manager {name}" in ret["comment"]

    # ReImport Certificate and update tags in real
    new_tags = [{"Key": "Name", "Value": "Updated tags and reimported"}]
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx=ctx,
        name=name,
        resource_id=certificate_arn_imported,
        private_key=crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode("utf-8"),
        certificate=crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode("utf-8"),
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert ret.get("old_state").get("resource_id") == ret.get("new_state").get(
        "resource_id"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"]["tags"]),
        new_tags,
    )
    assert f"Updated tags for aws.acm.certificate_manager '{name}'" in ret["comment"]

    # Delete imported certificate with test flag
    ret = await hub.states.aws.acm.certificate_manager.absent(
        test_ctx, name=certificate_arn_imported, resource_id=certificate_arn_imported
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Would delete aws.acm.certificate_manager '{certificate_arn_imported}'"
        in ret["comment"]
    )
    # Delete requested certificate with test flag
    ret = await hub.states.aws.acm.certificate_manager.absent(
        test_ctx, name=certificate_arn_requested, resource_id=certificate_arn_requested
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Would delete aws.acm.certificate_manager '{certificate_arn_requested}'"
        in ret["comment"]
    )

    # Delete imported certificate
    ret = await hub.states.aws.acm.certificate_manager.absent(
        ctx, name=certificate_arn_imported, resource_id=certificate_arn_imported
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Deleted aws.acm.certificate_manager '{certificate_arn_imported}'"
        in ret["comment"]
    )
    assert ret["old_state"] and not ret["new_state"]

    # Delete requested certificate
    ret = await hub.states.aws.acm.certificate_manager.absent(
        ctx, name=certificate_arn_requested, resource_id=certificate_arn_requested
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Deleted aws.acm.certificate_manager '{certificate_arn_requested}'"
        in ret["comment"]
    )
    assert ret["old_state"] and not ret["new_state"]

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.acm.certificate_manager.absent(
        ctx, name=certificate_arn_imported, resource_id=certificate_arn_imported
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.acm.certificate_manager '{certificate_arn_imported}' already absent"
        in ret["comment"]
    )
    assert not ret["old_state"] and not ret["new_state"]


@pytest.mark.asyncio
async def test_acm_absent_with_none_resource_id(hub, ctx):
    acm_temp_name = "idem-test-acm-" + str(uuid.uuid4())
    # Delete acm with resource_id as None. Result in no-op.
    ret = await hub.states.aws.acm.certificate_manager.absent(
        ctx, name=acm_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.acm.certificate_manager '{acm_temp_name}' already absent"
        in ret["comment"]
    )
