import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-volume-" + str(int(time.time())),
    "availability_zone": "ap-south-1a",
    "encrypted": True,
    "volume_type": "standard",
    "size": 1,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.ec2.volume.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert f"Would create aws.ec2.volume '{PARAMETER['name']}'" in ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert f"Created aws.ec2.volume '{PARAMETER['name']}'" in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["encrypted"] == resource.get("encrypted")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["size"] == resource.get("size")
    assert PARAMETER["volume_type"] == resource.get("volume_type")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.volume.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ec2.volume.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.ec2.volume.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    assert PARAMETER["size"] == described_resource_map.get("size")
    assert PARAMETER["encrypted"] == described_resource_map.get("encrypted")
    assert PARAMETER["volume_type"] == described_resource_map.get("volume_type")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.volume.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["volume_type"] == old_resource.get("volume_type")
    assert PARAMETER["size"] == old_resource.get("size")
    assert PARAMETER["encrypted"] == old_resource.get("encrypted")
    if __test:
        assert f"Would delete aws.ec2.volume '{PARAMETER['name']}'" in ret["comment"]
    else:
        assert f"Deleted aws.ec2.volume '{PARAMETER['name']}'" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.volume.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.volume '{PARAMETER['name']}' already absent" in ret["comment"]
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_absent_with_none_resource_id(hub, ctx):
    volume_temp_name = "idem-test-volume-" + str(int(time.time()))
    ret = await hub.states.aws.ec2.volume.absent(
        ctx, name=volume_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.volume '{volume_temp_name}' already absent" in ret["comment"]


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.volume.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
